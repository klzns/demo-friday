import React, { Component } from 'react';
import './App.css';
import Cover from './steps/Cover'
import Visitors from './steps/Visitors'
import Feedbacks from './steps/Feedbacks'
import TimeBombs from './steps/TimeBombs'
import KnownIssues from './steps/KnownIssues'
import Presentations from './steps/Presentations'
import AskGeraldoMariano from './steps/AskGeraldoMariano'
import RemoteViewers from './steps/RemoteViewers'
import FinalStep from './steps/FinalStep'

import Login from './Login/index'
import Header from './Header/index'
import mock from './mockResponse'

// Client ID and API key from the Developer Console
const CLIENT_ID = '748108146442-ed4frsp74gr0v5620o1n39i70fv74m0f.apps.googleusercontent.com';

// Array of API discovery doc URLs for APIs used by the quickstart
const DISCOVERY_DOCS = ["https://sheets.googleapis.com/$discovery/rest?version=v4"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
const SCOPES = "https://www.googleapis.com/auth/spreadsheets.readonly";

const isMockOn = true

class App extends Component {
  constructor() {
    super()

    if (isMockOn) {
      const [
        visitors,
        feedbacks,
        timeBombs,
        knownIssues,
        presentations,
        askGeraldoMariano,
        remoteViewers,
      ] = mock.valueRanges;

      this.state = {
        isSignedIn: true,
        isLoading: false,
        visitors,
        feedbacks,
        timeBombs,
        knownIssues,
        presentations,
        askGeraldoMariano,
        remoteViewers,
        sheet: '17/mar',
      }
    } else {
      this.state = {
        isSignedIn: null,
        isLoading: true,
      }
    }

  }

  componentDidMount() {
    document.addEventListener('GoogleSDKLoaded', this.onLoadGoogleSDK, false);
  }

  componentWillUmount() {
    document.removeEventListener(this.onLoadGoogleSDK)
  }

  onLoadGoogleSDK = () => {
    if (!isMockOn) {
      window.gapi.load('client:auth2', this.initClient);
    }
  }

  initClient = () => {
    window.gapi.client.init({
      discoveryDocs: DISCOVERY_DOCS,
      clientId: CLIENT_ID,
      scope: SCOPES
    })
      .then(() => {
        // Listen for sign-in state changes.
        window.gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);

        // Handle the initial sign-in state.
        this.updateSigninStatus(window.gapi.auth2.getAuthInstance().isSignedIn.get());;
      });
  }

  updateSigninStatus = (isSignedIn) => {
    this.getSheetsData()
    this.setState({ isSignedIn, isLoading: true })
  }

  handleAuthClick = () => {
    window.gapi.auth2.getAuthInstance().signIn();
  }

  handleSignoutClick = () => {
    window.gapi.auth2.getAuthInstance().signOut();
  }

  getSheetsData = () => {
    const DEMO_FRIDAY_SPREADSHEET_ID = '1lHg7wYzEyQC1mV9VeoSX_7L-AZx9QnnSYiugpzEzSpY'

  window.gapi.client.sheets.spreadsheets.get({
      spreadsheetId: DEMO_FRIDAY_SPREADSHEET_ID
    }).then((response) => {
      return response.result.sheets[0].properties.title
    }).then((sheet) => {
      const visitorsCells = `${sheet}!C13:G18`
      const feedbacksCells = `${sheet}!J13:K18`
      const timeBombsCells = `${sheet}!C26:F31`
      const knownIssuesCells = `${sheet}!J26:N31`
      const presentationsCells = `${sheet}!R13:T18`
      const askGeraldoMarianoCells = `${sheet}!R26:S31`
      const remoteViewersCells = `${sheet}!C40:C64`

      const cellDataPromise = window.gapi.client.sheets.spreadsheets.values.batchGet({
        spreadsheetId: DEMO_FRIDAY_SPREADSHEET_ID,
        ranges: [
          visitorsCells,
          feedbacksCells,
          timeBombsCells,
          knownIssuesCells,
          presentationsCells,
          askGeraldoMarianoCells,
          remoteViewersCells,
        ],
      })

      return Promise.all([sheet, cellDataPromise])
    }).then((_response) => {
      const [ sheet, response ] = _response
      const [
        visitors,
        feedbacks,
        timeBombs,
        knownIssues,
        presentations,
        askGeraldoMariano,
        remoteViewers,
      ] = response.result.valueRanges;

    this.setState({
        visitors,
        feedbacks,
        timeBombs,
        knownIssues,
        presentations,
        askGeraldoMariano,
        remoteViewers,
        sheet,
        isLoading: false,
      })
    }, (response) => {
      this.setState({
        error: response.result.error.message,
        isLoading: false,
      })
    });
  }

  render() {
    const { isSignedIn, isLoading, error } = this.state

    if (isSignedIn === false) {
      return <Login onClick={this.handleAuthClick} />
    }

    if (isSignedIn === null || isLoading) {
      return <div>Loading...</div>
    }

    if (error) {
      return <div>{error}</div>
    }

    return (
      <div className="App">
        <Header date={this.state.sheet} />
        <div>
					<Cover date={this.state.sheet} />
          <Visitors data={this.state.visitors} />
          <Feedbacks data={this.state.feedbacks} />
          <TimeBombs data={this.state.timeBombs} />
          <KnownIssues data={this.state.knownIssues} />
          <Presentations data={this.state.presentations} />
          <AskGeraldoMariano data={this.state.askGeraldoMariano} />
          <RemoteViewers data={this.state.remoteViewers} />
          <FinalStep />
        </div>
				<button id="signout-button" onClick={this.handleSignoutClick}>Até Mais!</button>
      </div>
    );
  }
}

export default App;
