import React from 'react'

export default function Chevron({ pointing }) {
  const symbol =
    pointing === 'down' ? `∨` :
    pointing === 'right' ? `›` :
    `›`

  return <span className="chevron">{symbol}</span>
}