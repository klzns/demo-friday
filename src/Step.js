import React, { Component } from 'react';
import './Step.css'

export default class Step extends Component {
  render() {
    const { title, children } = this.props

    return (
      <div className="step">
        <h1>{title}</h1>
        <div className="step-content">
          {children}
        </div>
      </div>
    )
  }
}