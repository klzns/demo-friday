module.exports = {
  "spreadsheetId": "1lHg7wYzEyQC1mV9VeoSX_7L-AZx9QnnSYiugpzEzSpY",
  "valueRanges": [
    {
      "range": "'17/mar'!C13:G18",
      "majorDimension": "ROWS",
      "values": [
        [
          "Eric Gallardo atualmente GSK e ex-Drogaria Onofre\n\nhttps://www.linkedin.com/in/ericgallardo/",
          "Eric Gallardo trabalhou na Drogaria Onofre e foi responsável pelo processo de seleção de uma plataforma de eCommerce para substituir solução própria, que foi desenvolvida a partir do código Vannon. Ele falará dos motivos que levou a escolher ATG e dos motivos que levou a não considerar VTEX. Atualmente Eric Gallardo está atuando na GSK ( http://br.gsk.com ) e está avaliando solução VTEX",
          "",
          "",
          "João Teixeira"
        ],
        [
          "Zeh Fernandes",
          "Dono da empresa de headphone.",
          "",
          "",
          "Breno Calazans"
        ]
      ]
    },
    {
      "range": "'17/mar'!J13:K18",
      "majorDimension": "ROWS",
      "values": [
        [
          "Nova feature no Checkout",
          "O Schirmer estava muito sério.."
        ],
        [
          "Nova feature no Checkout",
          "O Schirmer estava muito estranho.."
        ]
      ]
    },
    {
      "range": "'17/mar'!C26:F31",
      "majorDimension": "ROWS",
      "values": [
        [
          "Schirmer/Marcus",
          "Checkout",
          "Diminuir ao máximo a quantidade de sistemas no caminho crítico (começando pelo ProfileSystem) ",
          "Dividido em 4 etapas: \n1 - Remoção das chamadas ao Profile para salvar o cartão no fechamento de compra: EM STABLE\n2 - Remoção das chamadas ao Profile do botão de fechar compra: EM STABLE\n3 - Diminuir acoplamento e chamadas ao Profile: COM QA (PEQUENA PARTE JÁ EM STABLE)\n4 - Tirar dependência do Profile no carrinho: COM QA"
        ],
        [
          "Time do IAM",
          "*",
          "Apis com Set Cookie podem causar bad requests ao passarem do limite de tamanho de header",
          "Atualizar o pacote VTEXPractices.Net.Http para a versao 1.5.3\nCriado log para identificar os Sistemas que ainda enviam muitos Cookies\nPrazo para todos atualizarem:  25/1 (Quarta)"
        ]
      ]
    },
    {
      "range": "'17/mar'!J26:N31",
      "majorDimension": "ROWS",
      "values": [
        [
          "Geraldo",
          "Aumento dos erros 500 registrados pelo google bot",
          "Todas as lojas VTEX passaram a ter um aumento significativo de erro 500 registrados pelo Search Console do Google impactando a performance orgânica das lojas ",
          "",
          "https://splunk.vtex.com/en-US/app/search/search?q=search%20index%3Dcatalog%20bot%20sourcetype%3DLog%20type%3Derror&display.page.search.mode=smart&dispatch.sample_ratio=1&earliest=-7d%40h&latest=now&sid=1483727724.1453702"
        ]
      ]
    },
    {
      "range": "'17/mar'!R13:T18",
      "majorDimension": "ROWS",
      "values": [
        [],
        [
          "VtexAuthenticator",
          "Matheus"
        ],
        [],
        [],
        [],
        [
          "VTEXInsights",
          "Summers"
        ]
      ]
    },
    {
      "range": "'17/mar'!R26:S31",
      "majorDimension": "ROWS",
      "values": [
        [
          "Breno",
          "Gostou da nova forma de apresentar a demo friday?"
        ],
        [
          "Breno",
          "Gostou da nova forma de apresentar a demo friday?"
        ]
      ]
    },
    {
      "range": "'17/mar'!C40:C64",
      "majorDimension": "ROWS",
      "values": [
        [
          "fernando.henrique@vtex.com"
        ],
        [
          "Bastián Valenzuela"
        ],
        [
          "marco.paramo@vtex.com"
        ],
        [
          "sara.carbajo@vtex.com.br (VTEX Spain)"
        ],
        [
          "claudia.perales@vtex.com (Perú)"
        ],
        [
          "cecilia.cordoba@vtex.com "
        ],
        [
          "Buenos Aires Office"
        ],
        [
          "Patricia Oliveira"
        ],
        [
          "Fabio Barbosa VTEX COLOMBIA"
        ],
        [
          "Gustavo Giserman"
        ]
      ]
    }
  ]
}
