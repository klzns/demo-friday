import React, { Component } from 'react';

export default class Login extends Component {
  render() {
    return (
      <div className="login">
        <h1>You need to login first</h1>
        <button id="authorize-button" onClick={this.props.onClick}>Authorize</button>
      </div>
    )
  }
}