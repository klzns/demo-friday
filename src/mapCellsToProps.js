import React from 'react'
export default function mapCellsToProps(Component, mapFn) {
  return function WrappedComponent(props) {
    const newProps = mapFn(props)
    return <Component {...newProps} />
  }
}