import React, { Component } from 'react';
import './style.css'

export default class Header extends Component {
  render() {
    const date = this.props.date
    return (
      <div className="header line-bottom">
        <div className="header-title block-text">
          Demo Friday
        </div>
        <div className="header-date block-text">
          {date}
        </div>
      </div>
    )
  }
}