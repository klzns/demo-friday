import React from 'react'

export default function Cover({date}) {
  return (
    <div className="step">
			<div className="vcenter" style={{marginTop: '-2em'}}>
				<small className="date-cover">{date}</small>
				<h1 className="title-cover">#demofriday</h1>
			</div>
    </div>
  )
}
