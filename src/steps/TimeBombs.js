import React, { Component } from 'react'
import Step from '../Step'
import mapCellsToProps from '../mapCellsToProps'

class TimeBombs extends Component {
  render() {
    const data = this.props.data
    return (
      <Step title="Bombas Relógio">
        {
          data.length === 0
          ? <span>Nenhuma bomba relógio 😅</span>
          : (
            <div>
              {data.map((bomb, index) =>
                <div className="default-margin line-bottom" style={{paddingBottom: '1.3em'}} key={index}>
                  <div className="block-text">{bomb.assignee} ({bomb.service})</div>
                  <h3 className="compact-title">{bomb.name}</h3>
                  <pre className="long-text" style={{fontFamily: 'Fabriga'}}>{bomb.status}</pre>
                </div>
              )}
            </div>
          )
        }
      </Step>
    )
  }
}

export default mapCellsToProps(
  TimeBombs,
  (props) => {
    let mappedData = []
    if (props.data.values) {
      mappedData = props.data.values.map(bomb => ({
        assignee: bomb[0],
        service: bomb[1],
        name: bomb[2],
        status: bomb[3],
      }))
    }

    return {
      data: mappedData
    }
  }
)
