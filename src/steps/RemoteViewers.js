import React, { Component } from 'react';
import Step from '../Step'
import mapCellsToProps from '../mapCellsToProps'

class RemoteViewers extends Component {
  render() {
    const data = this.props.data
    return (
      <Step title="Remote Viewers">
        {
          data.length === 0
          ? <span>Nenhum remote viewer 👀</span>
          : (
            <ul className="list">
              {data.map((viewer, index) =>
                <li key={index}>
                  {viewer.name}
                </li>
              )}
            </ul>
          )
        }
      </Step>
    )
  }
}

export default mapCellsToProps(
  RemoteViewers,
  (props) => {
    let mappedData = []
    if (props.data.values) {
      mappedData = props.data.values.map(viewer => ({
        name: viewer[0],
      }))
    }

    return {
      data: mappedData
    }
  }
)
