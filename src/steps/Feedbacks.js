import React, { Component } from 'react';
import Step from '../Step'
import mapCellsToProps from '../mapCellsToProps'

class Feedbacks extends Component {
  render() {
    const data = this.props.data
    return (
      <Step title="Feedbacks">
        {
          data.length === 0
          ? <span>Sem feedbacks da última demo friday 💯</span>
          : (
            <div>
              {data.map((feedback, index) =>
                <div className="default-margin line-bottom" style={{paddingBottom: '1.3em'}} key={index}>
                  <div className="block-text" style={{ fontSize: '0.8em'}}>{feedback.presentation}</div>
                  <h3 className="compact-title">{feedback.feedback}</h3>
                </div>
              )}
            </div>
          )
        }
      </Step>
    )
  }
}

export default mapCellsToProps(
  Feedbacks,
  (props) => {
    let mappedData = []
    if (props.data.values) {
      mappedData = props.data.values.map(feedback => ({
        presentation: feedback[0],
        feedback: feedback[1],
      }))
    }

    return {
      data: mappedData
    }
  }
)
