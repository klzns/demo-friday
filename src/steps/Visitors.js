import React, { Component } from 'react'
import mapCellsToProps from '../mapCellsToProps'
import Step from '../Step'
import Chevron from '../Chevron'
import './Visitors.css'

class Visitors extends Component {
  constructor() {
    super()
    this.state = { openDescription: null }
  }

  handleClick = (index) => {
    this.setState({
      openDescription: this.state.openDescription === index ? null : index,
    })
  }

  render() {
    const data = this.props.data
    const isDescriptionOpen = this.state.isDescriptionOpen

    return (
      <Step title="Visitantes">
        {
          data.length === 0
          ? <span>Sem visitantes hoje  💁🏻‍♂️</span>
          : (
            <div>
              {data.map((visitor, index) =>
                <div className="visitors" key={index} onClick={() => this.handleClick(index)}>
                  <div className="visitors-head line-bottom">
                    <Chevron pointing={isDescriptionOpen ? 'down' : 'right'}/>&nbsp;
                    <span className="visitors-name">{visitor.name}</span>
                    <span className="visitors-host block-text">{visitor.host}</span>
                  </div>
                  <span className="visitors-description long-text" style={
                    { display: this.state.openDescription === index ? 'block' : 'none' }
                  }>
                    {visitor.description}
                  </span>
                </div>
              )}
            </div>
          )
        }
      </Step>
    )
  }
}

export default mapCellsToProps(
  Visitors,
  (props) => {
    let mappedData = []
    if (props.data.values) {
      mappedData = props.data.values.map(visitor => ({
        name: visitor[0],
        description: visitor[1],
        host: visitor[4],
      }))
    }

    return {
      data: mappedData
    }
  }
)
