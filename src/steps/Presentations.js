import React, { Component } from 'react';
import Step from '../Step'
import mapCellsToProps from '../mapCellsToProps'
import './Presentation.css'

class Presentations extends Component {
  render() {
    const data = this.props.data
    return (
      <Step title="Apresentações">
        {
          data.length === 0
          ? <span>Sem apresentações 😨</span>
          : (
            <div>
              {data.map((presentation, index) =>
                <div className="presentation line-bottom" key={index + 1}>
                  <div className="big-number vcenter">
                    0{index + 1}
                  </div>
                  <div className="presentation-info">
                    <span className="presentation-title">
                      {presentation.subject}
                    </span>
                    <span className="presentation-speaker block-text">
                      {presentation.speaker}
                    </span>
                  </div>
                </div>
              )}
            </div>
          )
        }
      </Step>
    )
  }
}

export default mapCellsToProps(
  Presentations,
  (props) => {
    let mappedData = []
    if (props.data.values) {
      mappedData = props.data.values.reduce((memo, presentation) => {
        if (!presentation[0] && !presentation[1]) {
          return memo
        }
        return memo.concat({
          subject: presentation[0],
          speaker: presentation[1],
          description: presentation[2],
        })
      }, [])
    }

    return {
      data: mappedData
    }
  }
)
