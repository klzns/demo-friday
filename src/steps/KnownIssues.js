import React, { Component } from 'react';
import Step from '../Step'
import mapCellsToProps from '../mapCellsToProps'

class KnownIssues extends Component {
  render() {
    const data = this.props.data
    return (
      <Step title="Problemas Conhecidos">
        {
          data.length === 0
          ? <span>Nenhum problema relatado 🔕</span>
          : (
            <div>
              {data.map((issue, index) =>
                <div key={index}>
                  <div className="block-text" style={{ fontSize: '0.8em'}}>{issue.assignee}</div>
                  <h3 className="compact-title">{issue.scenario}</h3>
                  <p className="long-text">{issue.impact}</p>
                </div>
              )}
            </div>
          )
        }
      </Step>
    )
  }
}

export default mapCellsToProps(
  KnownIssues,
  (props) => {
    let mappedData = []
    if (props.data.values) {
      mappedData = props.data.values.map(issue => ({
        assignee: issue[0],
        scenario: issue[1],
        impact: issue[2],
        tickets: issue[4],
      }))
    }

    return {
      data: mappedData
    }
  }
)