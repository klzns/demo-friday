import React, { Component } from 'react';
import Step from '../Step'
import mapCellsToProps from '../mapCellsToProps'

class AskGeraldoMariano extends Component {
  render() {
    const data = this.props.data
    return (
      <Step title="Ask Geraldo & Mariano">
        {
          data.length === 0
          ? <span>Sem perguntas  🔇</span>
          : (
            <div>
              {data.map((question, index) =>
                <div className="default-margin" key={index}>
                  <blockquote className="compact-title">{question.question}</blockquote>
                  <div className="block-text" style={{ fontSize: '0.8em'}}>- {question.who}</div>
                </div>
              )}
            </div>
          )
        }
      </Step>
    )
  }
}

export default mapCellsToProps(
  AskGeraldoMariano,
  (props) => {
    let mappedData = []
    if (props.data.values) {
      mappedData = props.data.values.map(question => ({
        who: question[0],
        question: question[1],
      }))
    }

    return {
      data: mappedData
    }
  }
)
